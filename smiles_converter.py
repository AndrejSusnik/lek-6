import numpy as np
import pandas as pd
import os
import rdkit  # if this fails, pray to god (it does not load in ipython)
from rdkit import Chem
from rdkit.Chem import Lipinski
from rdkit.Chem import Descriptors
from rdkit.ML.Descriptors import MoleculeDescriptors
from sklearn.preprocessing import quantile_transform, MinMaxScaler, OneHotEncoder

from CNN_smiles_article_source.feature import *

# featurevector size
atom_info = 21
struct_info = 21
len_size = atom_info + struct_info


def read_smiles_file(path_to_smiles, delimiter=" ", title_line=False):
    """
    Reads smiles file on path_to_smiles

    @param path_to_smiles: string path to folder with smiles data.

    Smiles file has to be without the header and space (' ') separated format.
    First column in smiles formula and the other is predicted feature.

    return np.array of Chem.rdchem.Mol data objects (molecules)
    """
    print("Reading smiles files")
    smiles = Chem.SmilesMolSupplier(
        path_to_smiles, delimiter=delimiter, titleLine=title_line
    )
    molecules = list(filter(None.__ne__, smiles))

    return molecules


def smiles_from_molfile(path_to_mol):
    return Chem.MolToSmiles(Chem.MolFromMolFile(path_to_mol))
    # data = open(path_to_mol, "r").read()
    # print(Chem.MolFromMolBlock(data))
    # suppl = Chem.SDMolSupplier(path_to_mol)
    # for mol in suppl:
    #    print(mol.GetNumAtoms())
    #    print(Chem.MolToSmiles(mol))


def molecules_to_features(
    list_of_molecules,
    max_len_smiles=400,
    kekuleSmiles=True,
    isomericSmiles=True,
    shuffle=False,
    property_name="_Name",
    seed=0,
    shady_quant=False,
    divide_100=False,
    into_10_classes=False,
    into_20_classes=False,
):
    """
    Convert Chem.rdchem.Mol into right smiles and then to one hot vector
    Does that for all molecules in the list and returns a set of data

    @param list_of_molecules: list of Chem.rdchem.Mol objects
    @max_len_smile: maximum lenght of smiles (for 0 padding)
                    if smiles longer; skip this smiles
    @kekuleSmiles: type of smiles representation (must be this for vector)
    @isomericSmiles: Smiles with isometry annotated (must be this for vector)
    @shuffle: shuffle outputed arrays (default False)
    @property_name: name of a target property (if False, return only X)

    return X, y np.arrays for CNN input (or X if property_name == False)
    """

    print("Converting molecules to features")
    F_list, T_list = [], []
    for mol in list_of_molecules:
        if (
            len(Chem.MolToSmiles(mol, kekuleSmiles=True, isomericSmiles=True))
            > max_len_smiles
        ):
            print("too long mol was ignored")
        else:
            F_list.append(mol_to_feature(mol, -1, max_len_smiles))
            if property_name == None:
                T_list.append(0)
            else:
                T_list.append(mol.GetProp(property_name))

    if shuffle:
        np.random.seed(seed)
        np.random.shuffle(F_list)
        np.random.seed(seed)
        np.random.shuffle(T_list)

    if "True" in T_list:
        T_list = [1 if t == "True" else 0 for t in T_list]

    data_target = np.asarray(T_list, dtype=np.float32).reshape(-1, 1)
    if shady_quant:
        data_target = MinMaxScaler().fit_transform(
            quantile_transform(data_target, output_distribution="uniform")
        )
        print(data_target)
    if divide_100:
        data_target = data_target / 100
    if into_10_classes:
        data_target = np.array([int(val // 10) + 0.5 for val in data_target]) / 10
    if into_20_classes:
        data_target = np.array([int(val // 34) + 0.5 for val in data_target]) / 3
        print(data_target)

    data_feature = np.asarray(F_list, dtype=np.float32).reshape(
        -1, 1, max_len_smiles, len_size
    )

    print(data_feature.shape, data_target.shape)

    return data_feature, data_target


def molecules_to_fingerprint(
    list_of_molecules, max_len_smiles=400, shuffle=False, seed=0
):
    """
    Convert molecules into binary fingerprint vectors.
    Bundle them into an array of shape (molecules, 2048)

    @param list_of_molecules: list of molecules Chem.rdchem.Mol

    return np.array with fingerprints of shape (molecules, 2048)
    """
    fingerprints = []

    print("Converting into fingerprints")

    for mol in list_of_molecules:
        if (
            len(Chem.MolToSmiles(mol, kekuleSmiles=True, isomericSmiles=True))
            > max_len_smiles
        ):
            print("too long mol was ignored")
        else:
            # binary sting
            fp = Chem.RDKFingerprint(mol).ToBitString()
            fingerprints += [np.array([bit for bit in fp], dtype=np.int32)]

    if shuffle:
        np.random.seed(seed)
        np.random.shuffle(fingerprints)

    return np.array(fingerprints)


def molecules_to_lipinski(list_of_molecules, max_len_smiles=400, shuffle=False, seed=0):
    """Get all features that are in rdkit.Chem.Lipinski module"""
    lipinski = []

    print("Converting into Lipinski features.")

    lipinski_functions = [
        Lipinski.FractionCSP3,
        Lipinski.HeavyAtomCount,
        Lipinski.NHOHCount,
        Lipinski.NOCount,
        Lipinski.NumAliphaticCarbocycles,
        Lipinski.NumAliphaticHeterocycles,
        Lipinski.NumAliphaticRings,
        Lipinski.NumAromaticCarbocycles,
        Lipinski.NumAromaticHeterocycles,
        Lipinski.NumAromaticRings,
        Lipinski.NumHAcceptors,
        Lipinski.NumHDonors,
        Lipinski.NumHeteroatoms,
        Lipinski.NumRotatableBonds,
        Lipinski.NumSaturatedCarbocycles,
        Lipinski.NumSaturatedHeterocycles,
        Lipinski.NumSaturatedRings,
        Lipinski.RingCount,
    ]

    for mol in list_of_molecules:
        if (
            len(Chem.MolToSmiles(mol, kekuleSmiles=True, isomericSmiles=True))
            > max_len_smiles
        ):
            print("too long mol was ignored")
        else:
            # apply all lipinski functions on a molecule
            mol_lipinski = []
            for i, lip_func in enumerate(lipinski_functions):
                mol_lipinski += [lip_func(mol)]

            lipinski += [mol_lipinski]

    return np.array(lipinski)


def calculate_features(list_of_molecules, max_len_smiles=400, shuffle=False, seed=0):
    """Get all features"""

    print("Calculating molecular descriptors.")

    mol_descriptors = []
    # print(Descriptors._descList)

    calc = MoleculeDescriptors.MolecularDescriptorCalculator(
        [x[0] for x in Descriptors._descList]
    )
    # print(calc.GetDescriptorNames())

    for mol in list_of_molecules:
        if (
            len(Chem.MolToSmiles(mol, kekuleSmiles=True, isomericSmiles=True))
            > max_len_smiles
        ):
            print("too long mol was ignored")
        else:
            mol_descriptors += [calc.CalcDescriptors(mol)]

    return np.nan_to_num(np.array(mol_descriptors))


if __name__ == "__main__":
    # read smiles file as a list of molecules
    molecules = read_smiles_file("datasets/TOX21/NR-AR-LBD_wholetraining.smiles")

    # then convert molecules into feature vector and target vector
    features, target = molecules_to_features(molecules)

    # convert molecules into fingerprints
    fps = molecules_to_fingerprint(molecules)

    # convert molecules into lipinski features
    lipinski = molecules_to_lipinski(molecules)
