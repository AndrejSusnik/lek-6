# Lek hack

## Smiles reader
Smiles reader read smiles from file and returns fingerprints
## CCNWrapper
Wrapper for keras models. New models should inherit from CNNWrapper and override build_model function.
See example_model.py for example.
Any parameters of the model have to be specified in NN_Params under model_type!
## NN_Params
Class for holding training and model parameters. Put into a model as a parameter, from which the model builds it's architecture. Changing parameters should be done with RandomGridSearch.
## RandomGridSearch
Class for randomly selecting training and model paramters. Yeilding from RandomGridSearch returns instance of NN_Params class that has random parameters from grid_search_dict. The instance can be a parameter for a new model.
### Dependencies
* numpy
* pandas
* os
* rdkit
* copy
* datetime
* tensorflow 2.0+
