import numpy as np
import pandas as pd
import tensorflow as tf
from datetime import datetime

# import whatever layers you need
from tensorflow.keras.layers import (
    Dense,
    Conv1D,
    Conv2D,
    Dropout,
    concatenate,
    MaxPooling1D,
    Flatten,
)
from tensorflow.keras import Input, Model, optimizers
from tensorflow.keras.callbacks import EarlyStopping
from sklearn.model_selection import train_test_split
from tensorflow.keras.models import load_model


from NN_Params import NNParams
from CNNWrapper import CNNWrapper


class TransferModel(CNNWrapper):
    def __init__(self, nn_params):
        super().__init__(nn_params)

        self.prefix_name = "tox_transfer_model"
        self.tp.optimizer = "sgd"
        self.tp.loss_func = "mse"
        self.tp.metrics = ["mae", "mape"]

    # Just override this function (at minimum)
    def build_model(self):

        tox_model = load_model(
            "models/chemixnet_cnn_lip/chemixnet_cnn_lip_25-9_22-35.h5"
        )
        tox_model.trainable = False
        # Pop last three layers
        layer1 = tox_model.layers.pop()
        layer2 = tox_model.layers.pop()
        layer3 = tox_model.layers.pop()

        # Add new final 2 dense layers
        new_dense = Dense(
            units=self.mp.dense_basic_1_units,
            activation=self.mp.dense_basic_1_act,
            name="basic_dense_1",
        )(tox_model.layers[-4].output)
        new_dense = Dense(
            units=self.mp.dense_basic_2_units,
            activation=self.mp.dense_basic_2_act,
            name="basic_dense_2",
        )(new_dense)
        new_final_layer = Dense(
            units=self.mp.final_dense_units,
            activation=self.mp.final_dense_act,
            name="final_dense",
        )(new_dense)

        # BUILDING A MODEL (functional API)
        self.model = Model(
            inputs=tox_model.input, outputs=new_final_layer, name=self.name
        )
