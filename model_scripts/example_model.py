import numpy as np
import pandas as pd
import tensorflow as tf
from datetime import datetime

print(tf.version)

# import whatever layers you need
from tensorflow.keras.layers import (
    Dense,
    Conv1D,
    Conv2D,
    Dropout,
    concatenate,
    Concatenate,
    LSTM,
    Flatten,
    MaxPooling1D,
)
from tensorflow.keras import Input, Model, optimizers
from tensorflow.keras.callbacks import EarlyStopping
from sklearn.model_selection import train_test_split

from NN_Params import NNParams
from CNNWrapper import CNNWrapper


class ExampleModel(CNNWrapper):
    def __init__(self, nn_params):
        super().__init__(nn_params)

        self.prefix_name = "example_model"

    # Just override this function (at minimum)
    def build_model(self):

        smiles = Input(shape=self.tp.smiles_shape)
        finger = Input(shape=self.tp.finger_shape)

        # one RNN layer for smiles
        lstm = LSTM(units=self.mp.lstm_units, activation=self.mp.lstm_act)(smiles)

        # one Dense layer for fingerprits
        f_dense = Dense(units=self.mp.dense_1_units, activation=self.mp.dense_1_act)(
            finger
        )

        concat = concatenate([lstm, f_dense], axis=1)

        final_dense = Dense(
            units=self.mp.final_dense_units, activation=self.mp.final_dense_act
        )(concat)

        # BUILDING A MODEL (functional API)
        self.model = Model(inputs=[smiles, finger], outputs=final_dense, name=self.name)


class CheMixNet_CNN(CNNWrapper):
    def __init__(self, nn_params):
        super().__init__(nn_params)

        self.prefix_name = "chemixnet_cnn"

    def build_model(self):

        smiles = Input(shape=self.tp.smiles_shape)
        finger = Input(shape=self.tp.finger_shape)

        # CNN 1D layer
        cnn_1D = smiles
        if self.mp.num_of_cnn_smiles >= 1:
            cnn_1D = Conv1D(
                filters=self.mp.cnn_1_filters,
                kernel_size=(self.mp.cnn_1_kernel_size,),
                activation=self.mp.cnn_1_act,
            )(cnn_1D)

            cnn_1D = MaxPooling1D(pool_size=self.mp.cnn_pool_size_1)(cnn_1D)

        if self.mp.num_of_cnn_smiles >= 2:
            cnn_1D = Conv1D(
                filters=self.mp.cnn_2_filters,
                kernel_size=self.mp.cnn_2_kernel_size,
                activation=self.mp.cnn_2_act,
            )(cnn_1D)

            cnn_1D = MaxPooling1D(pool_size=self.mp.cnn_pool_size_2)(cnn_1D)

        if self.mp.num_of_cnn_smiles >= 3:
            cnn_1D = Conv1D(
                filters=self.mp.cnn_3_filters,
                kernel_size=self.mp.cnn_3_kernel_size,
                activation=self.mp.cnn_3_act,
            )(cnn_1D)

            cnn_1D = MaxPooling1D(pool_size=self.mp.cnn_pool_size_3)(cnn_1D)

        cnn_1D = Flatten()(cnn_1D)

        # Finger DENSE layers
        f_dense = Dense(
            units=self.mp.f_dense_1_units, activation=self.mp.f_dense_1_act
        )(finger)

        f_dense = Dense(
            units=self.mp.f_dense_2_units, activation=self.mp.f_dense_2_act
        )(f_dense)

        f_dense = Dense(
            units=self.mp.f_dense_3_units, activation=self.mp.f_dense_3_act
        )(f_dense)

        if self.mp.num_of_dense_fingers >= 4:
            f_dense = Dense(
                units=self.mp.f_dense_4_units, activation=self.mp.f_dense_4_act
            )(f_dense)

        if self.mp.num_of_dense_fingers >= 5:
            f_dense = Dense(
                units=self.mp.f_dense_5_units, activation=self.mp.f_dense_5_act
            )(f_dense)

        # CONCATENATE
        concat = concatenate([cnn_1D, f_dense], axis=1)

        last = Dense(units=self.mp.l_dense_1_units, activation=self.mp.l_dense_1_act)(
            concat
        )

        if self.mp.num_of_dense_last >= 2:
            last = Dense(
                units=self.mp.l_dense_2_units, activation=self.mp.l_dense_2_act
            )(last)

        outputs = Dense(
            units=self.mp.output_dense_units, activation=self.mp.output_dense_act
        )(last)

        # BUILDING A MODEL (functional API)
        self.model = Model(inputs=[smiles, finger], outputs=outputs, name=self.name)


class CheMixNet_CNN_lip(CNNWrapper):
    def __init__(self, nn_params):
        super().__init__(nn_params)

        self.prefix_name = "chemixnet_cnn_lip"

    def build_model(self):

        smiles = Input(shape=self.tp.smiles_shape)
        finger = Input(shape=self.tp.finger_shape)
        lipinski = Input(shape=self.tp.lipinski_shape)

        # CNN 1D layer
        cnn_1D = smiles
        if self.mp.num_of_cnn_smiles >= 1:
            cnn_1D = Conv1D(
                filters=self.mp.cnn_1_filters,
                kernel_size=self.mp.cnn_1_kernel_size,
                activation=self.mp.cnn_1_act,
            )(cnn_1D)

            cnn_1D = MaxPooling1D(
                pool_size=self.mp.cnn_pool_size_1, strides=self.mp.cnn_1_stride
            )(cnn_1D)

        if self.mp.num_of_cnn_smiles >= 2:
            cnn_1D = Conv1D(
                filters=self.mp.cnn_2_filters,
                kernel_size=self.mp.cnn_2_kernel_size,
                activation=self.mp.cnn_2_act,
            )(cnn_1D)

            cnn_1D = MaxPooling1D(
                pool_size=self.mp.cnn_pool_size_2, strides=self.mp.cnn_2_stride
            )(cnn_1D)

        if self.mp.num_of_cnn_smiles >= 3:
            cnn_1D = Conv1D(
                filters=self.mp.cnn_3_filters,
                kernel_size=self.mp.cnn_3_kernel_size,
                activation=self.mp.cnn_3_act,
            )(cnn_1D)

            cnn_1D = MaxPooling1D(
                pool_size=self.mp.cnn_pool_size_3, strides=self.mp.cnn_3_stride
            )(cnn_1D)

        cnn_1D = Flatten()(cnn_1D)

        cnn_1D = Dense(units=self.mp.cnn_dense_units, activation=self.mp.cnn_dense_act)(
            cnn_1D
        )
        cnn_1D = Dropout(rate=self.mp.cnn_dense_drop)(cnn_1D)

        # Finger DENSE layers
        f_dense = Dense(
            units=self.mp.f_dense_1_units, activation=self.mp.f_dense_1_act
        )(finger)

        f_dense = Dense(
            units=self.mp.f_dense_2_units, activation=self.mp.f_dense_2_act
        )(f_dense)

        f_dense = Dense(
            units=self.mp.f_dense_3_units, activation=self.mp.f_dense_3_act
        )(f_dense)

        if self.mp.num_of_dense_fingers >= 4:
            f_dense = Dense(
                units=self.mp.f_dense_4_units, activation=self.mp.f_dense_4_act
            )(f_dense)

        if self.mp.num_of_dense_fingers >= 5:
            f_dense = Dense(
                units=self.mp.f_dense_5_units, activation=self.mp.f_dense_5_act
            )(f_dense)

        # Lipinski DENSE layers
        lip_dense = Dense(
            units=self.mp.lip_dense_1_units, activation=self.mp.lip_dense_1_act
        )(lipinski)

        lip_dense = Dense(
            units=self.mp.lip_dense_2_units, activation=self.mp.lip_dense_2_act
        )(lip_dense)

        # CONCATENATE
        concat = Concatenate(axis=1)([cnn_1D, f_dense, lip_dense])

        last = Dense(
            units=self.mp.l_dense_1_units,
            activation=self.mp.l_dense_1_act,
        )(concat)
        last = Dropout(rate=self.mp.l_dense_1_drop)(last)

        if self.mp.num_of_dense_last >= 2:
            last = Dense(
                units=self.mp.l_dense_2_units,
                activation=self.mp.l_dense_2_act,
            )(last)
            last = Dropout(rate=self.mp.l_dense_2_drop)(last)

        outputs = Dense(
            units=self.mp.output_dense_units, activation=self.mp.output_dense_act
        )(last)

        # BUILDING A MODEL (functional API)
        self.model = Model(
            inputs=[smiles, finger, lipinski], outputs=outputs, name=self.name
        )


class CheMixNet_CNN_mol(CNNWrapper):
    def __init__(self, nn_params):
        super().__init__(nn_params)

        # Copy of chemixnet_cnn_lip with added Features
        self.prefix_name = "chemixnet_cnn_feat"

    def build_model(self):

        smiles = Input(shape=self.tp.smiles_shape)
        finger = Input(shape=self.tp.finger_shape)
        lipinski = Input(shape=self.tp.lipinski_shape)
        mol_features = Input(shape=self.tp.features_shape)

        # CNN 1D layer
        cnn_1D = smiles
        if self.mp.num_of_cnn_smiles >= 1:
            cnn_1D = Conv1D(
                filters=self.mp.cnn_1_filters,
                kernel_size=self.mp.cnn_1_kernel_size,
                activation=self.mp.cnn_1_act,
            )(cnn_1D)

            cnn_1D = MaxPooling1D(
                pool_size=self.mp.cnn_pool_size_1, strides=self.mp.cnn_1_stride
            )(cnn_1D)

        if self.mp.num_of_cnn_smiles >= 2:
            cnn_1D = Conv1D(
                filters=self.mp.cnn_2_filters,
                kernel_size=self.mp.cnn_2_kernel_size,
                activation=self.mp.cnn_2_act,
            )(cnn_1D)

            cnn_1D = MaxPooling1D(
                pool_size=self.mp.cnn_pool_size_2, strides=self.mp.cnn_2_stride
            )(cnn_1D)

        if self.mp.num_of_cnn_smiles >= 3:
            cnn_1D = Conv1D(
                filters=self.mp.cnn_3_filters,
                kernel_size=self.mp.cnn_3_kernel_size,
                activation=self.mp.cnn_3_act,
            )(cnn_1D)

            cnn_1D = MaxPooling1D(
                pool_size=self.mp.cnn_pool_size_3, strides=self.mp.cnn_3_stride
            )(cnn_1D)

        cnn_1D = Flatten()(cnn_1D)

        cnn_1D = Dense(units=self.mp.cnn_dense_units, activation=self.mp.cnn_dense_act)(
            cnn_1D
        )

        # Finger DENSE layers
        f_dense = Dense(
            units=self.mp.f_dense_1_units, activation=self.mp.f_dense_1_act
        )(finger)

        f_dense = Dense(
            units=self.mp.f_dense_2_units, activation=self.mp.f_dense_2_act
        )(f_dense)

        f_dense = Dense(
            units=self.mp.f_dense_3_units, activation=self.mp.f_dense_3_act
        )(f_dense)

        if self.mp.num_of_dense_fingers >= 4:
            f_dense = Dense(
                units=self.mp.f_dense_4_units, activation=self.mp.f_dense_4_act
            )(f_dense)

        if self.mp.num_of_dense_fingers >= 5:
            f_dense = Dense(
                units=self.mp.f_dense_5_units, activation=self.mp.f_dense_5_act
            )(f_dense)

        # Lipinski DENSE layers
        lip_dense = Dense(
            units=self.mp.lip_dense_1_units, activation=self.mp.lip_dense_1_act
        )(lipinski)

        lip_dense = Dense(
            units=self.mp.lip_dense_2_units, activation=self.mp.lip_dense_2_act
        )(lip_dense)

        # Molecular features DENSE layers
        mol_dense = Dense(
            units=self.mp.mol_dense_1_units, activation=self.mp.mol_dense_1_act
        )(mol_features)

        mol_dense = Dense(
            units=self.mp.mol_dense_2_units, activation=self.mp.mol_dense_2_act
        )(mol_dense)

        # CONCATENATE
        concat = Concatenate(axis=1)([cnn_1D, f_dense, lip_dense, mol_dense])

        last = Dense(
            units=self.mp.l_dense_1_units,
            activation=self.mp.l_dense_1_act,
        )(concat)

        if self.mp.num_of_dense_last >= 2:
            last = Dense(
                units=self.mp.l_dense_2_units,
                activation=self.mp.l_dense_2_act,
            )(last)

        outputs = Dense(
            units=self.mp.output_dense_units, activation=self.mp.output_dense_act
        )(last)

        # BUILDING A MODEL (functional API)
        self.model = Model(
            inputs=[smiles, finger, lipinski, mol_features],
            outputs=outputs,
            name=self.name,
        )


class Final_Fingers(CNNWrapper):
    def __init__(self, nn_params):
        super().__init__(nn_params)

        self.prefix_name = "final_fingers"

    def build_model(self):

        smiles = Input(shape=self.tp.smiles_shape)
        finger = Input(shape=self.tp.finger_shape)
        lipinski = Input(shape=self.tp.lipinski_shape)

        # Finger DENSE layers
        f_dense = Dense(
            units=self.mp.f_dense_1_units, activation=self.mp.f_dense_1_act
        )(finger)
        f_dense = Dropout(rate=self.mp.f_dense_1_drop)(f_dense)

        f_dense = Dense(
            units=self.mp.f_dense_2_units, activation=self.mp.f_dense_2_act
        )(f_dense)
        f_dense = Dropout(rate=self.mp.f_dense_1_drop)(f_dense)

        f_dense = Dense(
            units=self.mp.f_dense_3_units, activation=self.mp.f_dense_3_act
        )(f_dense)
        f_dense = Dropout(rate=self.mp.f_dense_1_drop)(f_dense)

        if self.mp.num_of_dense_fingers >= 4:
            f_dense = Dense(
                units=self.mp.f_dense_4_units, activation=self.mp.f_dense_4_act
            )(f_dense)
        f_dense = Dropout(rate=self.mp.f_dense_1_drop)(f_dense)

        if self.mp.num_of_dense_fingers >= 5:
            f_dense = Dense(
                units=self.mp.f_dense_5_units, activation=self.mp.f_dense_5_act
            )(f_dense)
        f_dense = Dropout(rate=self.mp.f_dense_1_drop)(f_dense)

        outputs = Dense(
            units=self.mp.output_dense_units, activation=self.mp.output_dense_act
        )(f_dense)

        # BUILDING A MODEL (functional API)
        self.model = Model(
            inputs=[smiles, finger, lipinski], outputs=outputs, name=self.name
        )