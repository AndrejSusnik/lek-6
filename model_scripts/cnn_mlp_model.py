import numpy as np
import pandas as pd
import tensorflow as tf
from datetime import datetime

# import whatever layers you need
from tensorflow.keras.layers import (
    Dense,
    Conv1D,
    Conv2D,
    Dropout,
    concatenate,
    MaxPooling1D,
    Flatten,
)
from tensorflow.keras import Input, Model, optimizers
from tensorflow.keras.callbacks import EarlyStopping
from sklearn.model_selection import train_test_split

from NN_Params import NNParams
from CNNWrapper import CNNWrapper


class cnnMlpModel(CNNWrapper):
    def __init__(self, nn_params):
        super().__init__(nn_params)

        self.prefix_name = "cnn_mlp_model"
        self.tp.optimizer = "adam"
        self.tp.loss_func = "binary_crossentropy"
        self.tp.metrics = "acc"


    # Just override this function (at minimum)
    def build_model(self):

        smiles = Input(shape=self.tp.smiles_shape)
        fingerprints = Input(shape=self.tp.finger_shape)

        # CNN
        conv_1 = Conv1D(
            filters=self.mp.cnn_1_filters,
            kernel_size=self.mp.cnn_1_kernel_size,
            padding="same",
            activation=self.mp.cnn_1_act,
        )(smiles)

        pool_1 = MaxPooling1D(pool_size=2)(conv_1)

        conv_2 = Conv1D(
            filters=self.mp.cnn_1_filters,
            kernel_size=self.mp.cnn_1_kernel_size,
            padding="same",
            activation=self.mp.cnn_1_act,
        )(pool_1)

        max_pool = MaxPooling1D(pool_size=2)(conv_2)

        flat = Flatten()(max_pool)
        cnn_dense = Dense(self.mp.dense_basic_units)(flat)

        # Fingerprints
        fc_1 = Dense(
            self.mp.dense_1_units, activation=self.mp.dense_1_act, input_dim=167
        )(fingerprints)
        fc_2 = Dense(self.mp.dense_1_units, activation=self.mp.dense_2_act)(fc_1)
        fc_dense = Dense(self.mp.dense_basic_units, activation=tf.nn.relu)(fc_2)

        # Merged
        concat = concatenate([cnn_dense, fc_dense], axis=1)
        dense = Dense(self.mp.dense_basic_units)(concat)
        final_dense = Dense(
            units=self.mp.final_dense_units, activation=self.mp.final_dense_act,
        )(dense)

        # BUILDING A MODEL (functional API)
        self.model = Model(
            inputs=[smiles, fingerprints], outputs=final_dense, name=self.name
        )

