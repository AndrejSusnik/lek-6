"""MAIN SCRIPT FOR ANN TRAINING"""
import numpy as np
import pandas as pd
import tensorflow as tf

# our own imports
from NN_Params import NNParams
from CNNWrapper import CNNWrapper
from model_scripts.transfer_model import TransferModel  # soon (*)
from smiles_converter import *
from random_grid_search import RandomGridSearch

# add your custom model in this dictionary
model_type_dict = {
    "tox_transfer_model": TransferModel,
}


def train_models(
    model_type,
    N_ITERATIONS,
    data_dict,
    stable_dict={},
    grid_search_dict={},
    save_model=False,
    write_log=True,
    shady_quant=False,
    divide_100=False,
):
    """FROM HERE ON OUT EVERYTHING SHOULD BE AUTOMATED"""

    molecules = read_smiles_file(
        data_dict["path_to_smiles"],
        delimiter=data_dict["delimiter"],
        title_line=data_dict["title_line"],
    )

    # transform molecules into training dataset
    smiles, y_labels = molecules_to_features(
        molecules, shady_quant=shady_quant, divide_100=divide_100
    )
    fingers = molecules_to_fingerprint(molecules)
    smiles = smiles.reshape(len(smiles), 400, 42)
    lipinski = molecules_to_lipinski(molecules)

    if N_ITERATIONS <= 0:
        N_ITERATIONS = 1
        for key, val in grid_search_dict.items():
            N_ITERATIONS *= len(val)
    if N_ITERATIONS >= 10 ** 5:
        N_ITERATIONS = 10 ** 5

    nn_params = NNParams(model_type=model_type, dataset=data_dict["path_to_smiles"])
    rnd_grid = RandomGridSearch(nn_params, grid_search_dict, N_ITERATIONS)

    # set epochs and batch_size
    for name, param in stable_dict.items():
        setattr(nn_params.train_params, name, param)

    for count in range(N_ITERATIONS):

        # call random grid and get new nn_params dict
        new_nn_params = next(rnd_grid.new_nn_params)

        model_class = model_type_dict[model_type](new_nn_params)
        model_class.run_training(
            smiles=smiles,
            fingers=fingers,
            props=lipinski,
            y_labels=y_labels,
            save_model=save_model,
            write_log=write_log,
        )


if __name__ == "__main__":
    """DEFINE EVERTHING YOU LIKE HERE"""
    # define this as a model you would like to train

    MODEL_TYPE = "tox_transfer_model"
    N_ITERATIONS = -1  # set -1 if you want all

    # dictionary for stable changes to the training parameters
    stable_train_params = {
        "NUM_EPOCHS": 50,
        "BATCH_SIZE": 32,
        "loss_func": "mse",
        "metrics": ["mae", "mape"],
        "verbose": 2,
        "standard_props": False,
    }

    # dictionary for data import
    data_dict = {
        "path_to_smiles": "dataset_lek/smiles_lek.csv",
        "delimiter": ";",
        "title_line": False,
    }

    # dictionary for grid search, changing model parameters
    grid_search_dict = {
        "dense_basic_1_units": [5, 10, 20, 30, 50],
        "dense_basic_1_act": ["tanh", "elu"],
        "dense_basic_2_units": [2, 5, 7, 10],
        "dense_basic_2_act": ["tanh", "elu"],
    }

    train_models(
        model_type=MODEL_TYPE,
        N_ITERATIONS=N_ITERATIONS,
        data_dict=data_dict,
        stable_dict=stable_train_params,
        grid_search_dict=grid_search_dict,
        save_model=False,
        write_log=True,
        divide_100=True,
    )
