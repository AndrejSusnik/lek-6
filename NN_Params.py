import numpy as np
import pandas as pd


class NNParams:
    def __init__(self, model_type="wrapper", dataset=None):

        self.model_type = model_type
        self.dataset = dataset

        self.model_params = self.ModelParams(self)

        self.train_params = self.TrainParams()

    class TrainParams:

        NUM_EPOCHS = 30
        BATCH_SIZE = 64
        verbose = 1  # 0-none, 1-all, 2-after each epoch
        save_test_set = False

        test_size = 0.20
        val_size = 0.20
        shuffle_train_test = True
        standard_props = True
        random_state = 42

        loss_func = "mse"
        metrics = "mae"

        # additional parameters for training
        one_cycle_learning = False
        one_cycle_max_rate = 10 ** -2
        one_cycle_min_rate = 10 ** -5
        one_cycle_end_rate = 10 ** -6

        learning_rate = 10 ** -2
        momentum = 0.9

        early_stopping = True
        patience = 10

        # optimizer settings
        optimizer = "sgd"
        nesterov = True

        # data shapes
        smiles_shape = (400, 42)
        finger_shape = (2048,)
        lipinski_shape = (18,)
        features_shape = (200,)

    class ModelParams:
        def __init__(self, nn_params):
            self.model_type = nn_params.model_type

            # add elif statement and add other parameters
            if self.model_type == "wrapper":

                # Final DENSE layer
                self.final_dense_units = 1
                self.final_dense_act = "sigmoid"

            elif self.model_type == "example_model":

                # One RNN layer
                self.lstm_units = 30
                self.lstm_act = "tanh"

                # Oner Dense layer
                self.dense_1_units = 50
                self.dense_1_act = "relu"

                # Final dense
                self.final_dense_units = 1
                self.final_dense_act = "sigmoid"

            elif self.model_type == "chemixnet_cnn":

                # CNN 1D layer
                self.num_of_cnn_smiles = 1  # 1-3

                self.cnn_1_kernel_size = 3
                self.cnn_1_filters = 100
                self.cnn_1_act = "elu"
                self.cnn_pool_size_1 = 2

                self.cnn_2_filters = 64
                self.cnn_2_kernel_size = 3
                self.cnn_2_act = "elu"
                self.cnn_pool_size_2 = 2

                self.cnn_3_filters = 16
                self.cnn_3_kernel_size = 1
                self.cnn_3_act = "elu"
                self.cnn_pool_size_3 = 1

                self.cnn_dense_units = 5
                self.cnn_dense_act = "elu"

                # FC finger layer
                self.num_of_dense_fingers = 3  # 3-5

                self.f_dense_1_units = 256
                self.f_dense_1_act = "tanh"

                self.f_dense_2_units = 128
                self.f_dense_2_act = "elu"

                self.f_dense_3_units = 64
                self.f_dense_3_act = "elu"

                self.f_dense_4_units = 32
                self.f_dense_4_act = "elu"

                self.f_dense_5_units = 16
                self.f_dense_5_act = "elu"

                # Last DENSE layers
                self.num_of_dense_last = 1  # 1-2

                self.l_dense_1_units = 16
                self.l_dense_1_act = "tanh"

                self.l_dense_2_units = 10
                self.l_dense_2_act = "tanh"

                # output DENSE layer
                self.output_dense_units = 1
                self.output_dense_act = "sigmoid"

            elif self.model_type == "chemixnet_cnn_lip":

                # CNN 1D layer
                self.num_of_cnn_smiles = 1  # 1-3

                self.cnn_1_filters = 100
                self.cnn_1_kernel_size = 3
                self.cnn_1_act = "elu"
                self.cnn_pool_size_1 = 2
                self.cnn_1_stride = 1

                self.cnn_2_filters = 64
                self.cnn_2_kernel_size = 3
                self.cnn_2_act = "elu"
                self.cnn_pool_size_2 = 2
                self.cnn_2_stride = 1

                self.cnn_3_filters = 16
                self.cnn_3_kernel_size = 1
                self.cnn_3_act = "elu"
                self.cnn_pool_size_3 = 1
                self.cnn_3_stride = 1

                self.cnn_dense_units = 100
                self.cnn_dense_act = "elu"
                self.cnn_dense_drop = 0.0

                # FC finger layer
                self.num_of_dense_fingers = 3  # 3-5

                self.f_dense_1_units = 256
                self.f_dense_1_act = "tanh"

                self.f_dense_2_units = 128
                self.f_dense_2_act = "elu"

                self.f_dense_3_units = 64
                self.f_dense_3_act = "elu"

                self.f_dense_4_units = 32
                self.f_dense_4_act = "elu"

                self.f_dense_5_units = 16
                self.f_dense_5_act = "elu"

                # Lipinski DENSE layers
                self.lip_dense_1_units = 10
                self.lip_dense_1_act = "tanh"

                self.lip_dense_2_units = 5
                self.lip_dense_2_act = "tanh"

                # Last DENSE layers
                self.num_of_dense_last = 1  # 1-2

                self.l_dense_1_units = 16
                self.l_dense_1_act = "tanh"
                self.l_dense_1_drop = 0.0

                self.l_dense_2_units = 10
                self.l_dense_2_act = "tanh"
                self.l_dense_2_drop = 0.0

                # output DENSE layer
                self.output_dense_units = 1
                self.output_dense_act = "sigmoid"

            elif self.model_type == "cnn_mlp_model":

                # First CNN
                self.cnn_1_filters = 32
                self.cnn_1_kernel_size = 3
                self.cnn_1_act = "relu"

                # First fingerprints dense
                self.dense_1_units = 512
                self.dense_1_act = "relu"

                # Second fingerprints dense
                self.dense_2_units = 256
                self.dense_2_act = "relu"

                # Basic  DENSE
                self.dense_basic_units = 64

                # Final dense
                self.final_dense_units = 1
                self.final_dense_act = "sigmoid"

            elif self.model_type == "chemixnet_cnn_feat":

                # CNN 1D layer
                self.num_of_cnn_smiles = 1  # 1-3

                self.cnn_1_filters = 100
                self.cnn_1_kernel_size = 3
                self.cnn_1_act = "elu"
                self.cnn_pool_size_1 = 2
                self.cnn_1_stride = 1

                self.cnn_2_filters = 64
                self.cnn_2_kernel_size = 3
                self.cnn_2_act = "elu"
                self.cnn_pool_size_2 = 2
                self.cnn_2_stride = 1

                self.cnn_3_filters = 16
                self.cnn_3_kernel_size = 1
                self.cnn_3_act = "elu"
                self.cnn_pool_size_3 = 1
                self.cnn_3_stride = 1

                # FC finger layer
                self.num_of_dense_fingers = 3  # 3-5

                self.f_dense_1_units = 256
                self.f_dense_1_act = "tanh"

                self.f_dense_2_units = 128
                self.f_dense_2_act = "elu"

                self.f_dense_3_units = 64
                self.f_dense_3_act = "elu"

                self.f_dense_4_units = 32
                self.f_dense_4_act = "elu"

                self.f_dense_5_units = 16
                self.f_dense_5_act = "elu"

                # Lipinski DENSE layers
                self.lip_dense_1_units = 10
                self.lip_dense_1_act = "tanh"

                self.lip_dense_2_units = 5
                self.lip_dense_2_act = "tanh"

                # Molecular features DENSE layers
                self.mol_dense_1_units = 10
                self.mol_dense_1_act = "tanh"

                self.mol_dense_2_units = 5
                self.mol_dense_2_act = "tanh"

                # Last DENSE layers
                self.num_of_dense_last = 1  # 1-2

                self.l_dense_1_units = 16
                self.l_dense_1_act = "tanh"
                self.l_dense_1_drop = 0.0

                self.l_dense_2_units = 10
                self.l_dense_2_act = "tanh"
                self.l_dense_2_drop = 0.0

                # output DENSE layer
                self.output_dense_units = 1
                self.output_dense_act = "sigmoid"

            elif self.model_type == "cnn_mlp_model":

                # First CNN
                self.cnn_1_filters = 32
                self.cnn_1_kernel_size = 3
                self.cnn_1_act = "relu"

                # First fingerprints dense
                self.dense_1_units = 512
                self.dense_1_act = "relu"

                # Second fingerprints dense
                self.dense_2_units = 256
                self.dense_2_act = "relu"

                # Basic  DENSE
                self.dense_basic_units = 64

                # Final dense
                self.final_dense_units = 1
                self.final_dense_act = "sigmoid"

            elif self.model_type == "tox_transfer_model":

                # Basic  DENSE
                self.dense_basic_1_units = 20
                self.dense_basic_1_act = "elu"

                self.dense_basic_2_units = 10
                self.dense_basic_2_act = "elu"

                # Final dense
                self.final_dense_units = 1
                self.final_dense_act = "sigmoid"

            elif self.model_type == "final_fingers":

                # FC finger layer
                self.num_of_dense_fingers = 5  # 3-5

                self.f_dense_1_units = 256
                self.f_dense_1_act = "elu"
                self.f_dense_1_drop = 0.0

                self.f_dense_2_units = 128
                self.f_dense_2_act = "elu"
                self.f_dense_2_drop = 0.0

                self.f_dense_3_units = 64
                self.f_dense_3_act = "elu"
                self.f_dense_3_drop = 0.0

                self.f_dense_4_units = 32
                self.f_dense_4_act = "elu"
                self.f_dense_4_drop = 0.0

                self.f_dense_5_units = 16
                self.f_dense_5_act = "elu"
                self.f_dense_5_drop = 0.0

                # Final dense
                self.final_dense_units = 1
                self.final_dense_act = "sigmoid"

            else:
                raise KeyError(f"{self.model_type} is not a valid model type.")
